#! /usr/bin/env python3
# just testing markdown library for now

from markdown import markdown as to_markdown

blog = ""

with open('blogs/Lorem Ipsum.md','rt') as file:
    blog = file.read()

with open('blogs/Lorem Ipsum.html', 'wt') as file:
    file.write(to_markdown(blog))
